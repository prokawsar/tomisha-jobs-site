/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			colors: {
				txtGray: '#718096',
				txtPrimary: '#319795',
				cBorder: '#CBD5E0'
			}
		}
	},
	plugins: []
}
